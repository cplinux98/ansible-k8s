目标：

​	使用ansible自动化部署k8s相关环境



现状：

- [x] 自动完成dns、dns-client安装与配置
  - [x] 可以根据长主机名、短主机名与其他主机互联
- [x] 自动完成docker的安装与配置
- [x] 自动完成harbor的安装与配置
  - [x] 升级https访问
- [x] 自动完成k8s-master单节点的安装与配置
- [x] 自动完成k8s-node节点的自动加入



预期实现：

- [ ] 自动完成k8s-master集群配置
- [ ] 自动安装dashboard并输出token
- [ ] 将固定的ip、镜像地址等信息改为变量，通过group_vars统一管控


操作步骤：

基础架构

1. 初始化group_vars内部变量
2. 编辑hosts主机组内信息
3. 使用dns.yml安装bind服务并根据hosts生成对应的主机记录
4. 使用harbor.yml安装harbor服务器
5. 使用xx.sh脚本下载k8s相关镜像并上传到harbor私有仓库



k8s集群相关

1. kube_all_init.yml 初始化所有k8s节点
2. kube_master_install.yml  安装k8s-master01
3. kube_node_install.yml 配置node01-node03节点
4. kube_node_join.yml 节点加入k8s-master01


