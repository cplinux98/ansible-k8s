#!/bin/bash
host_array=(kubeadm-master01 kubeadm-master02 kubeadm-master03 kubeadm-node01 kubeadm-node02 kubeadm-node03)


for i in ${host_array[@]}
do
expect -c "
spawn ssh-copy-id -i /root/.ssh/id_rsa.pub root@$i
	expect {
				\"*yes/no*\" {send \"yes\r\"; exp_continue}
				\"*password*\" {send \"123456\r\"; exp_continue}
				\"*Password*\" {send \"123456\r\";}
	} "
done
